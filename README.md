# Project Colomba Livia

[![](https://stlcom.com/wp-content/uploads/2016/03/mkfjKWkA_400x400.jpg)](https://en.wikipedia.org/wiki/War_pigeon)

> The pattern "Publisher-Subscriber" was implemented by tank drivers using pigeons prior to radio as an attempt to create a distributed messaging system [Wikipedia](https://en.wikipedia.org/wiki/Publish%E2%80%93subscribe_pattern). 

## About

https://en.wikipedia.org/wiki/RSS

**Requirements**

- User account creation and login
- Support to follow multiple feeds with at least support for the following ones
- Ability to add a bookmark/favorite to a feed item.
- Ability to add Markdown based comments to the feed items.

## Welcome!

Please see Gitlab Pages before
continuing: [Project Columba Livia](https://matyas-banszki-test-assignments.gitlab.io/project-columba-livia/)

### References

- https://en.wikipedia.org/wiki/Behavior-driven_development
- https://cucumber.io/

### Personal Reference

- http://dtective.io/

## Table of Content

* [Repository Structure](#repository-structure)
* [Quickstart](#quickstart)
* [Testing](#testing-strategies)
    * [Behavior Driven Testing](#behavior-driven-testing)
    * [Data Driven Testing](#data-driven-testing)
    * [Performance Testing](#performance-testing)
    * [Test Suites](#test-suites)
        * [Sanity](#sanity-testing)
        * [Smoke](#smoke-testing)
        * [Regression](#regression-testing)
* [Test Files](#test-files)
    * [Features](#features)
    * [Page Objects](#page-objects)
* [Tools and Technologies](#tools-and-technologies)
    * [Docker](#docker-integration)
    * [Selenium](#selenium-integration)
    * [Elastic Stack ELK](#elk-integration)
    * [Gitlab](#gitlab-integration)
    * [IntelliJ IDEA](#intellij-integration)
    * [Google Lighthouse](#google-lighthouse-integration)
    * [Apache Maven](#maven-integration)
    * [Sitespeed.io](#sitespeed-integration)
* [Analytics](#tools-and-technologies)
    * [Framework Analytics](#framework-analytics)

## Repository Structure

```text
    .
    ├── config                  # Configuration files (Framework and Integrations)
    ├── docs                    # Documentation files (Jekyll)
    ├── steps                   # Java Source files (Cucumber)
    ├── test                    # Automated tests (alternatively `spec` or `tests`)
    ├── resources               # Test Configs and resources
    ├── tools                   # Tools and utilities
    ├── .dockerignore           # Files to be ignored during docker builds
    ├── .env.example            # Suggestion for environment variables in docker
    ├── .gitlab-ci.yml          # Templated pipeline file
    ├── docker-compose.yml      # Docker service deployment file
    ├── Makefile                # Command aggregator
    ├── pom.xml                 # Maven Project Configuration
    ├── settings.xml            # Template Maven Settings file to access private package repositories
    └── README.md
```

## Quickstart!

### Pre-Requisites

- The project will not build without a private Package Manager API token.
- The project relies on tools and technologies further described in their respective sections. This Quickstart relies
  on https://www.docker.com/

> _Make your life easier with 'Makefile'!_

Run the following commands, see definition and details in **Makefile**

- `make quickstart`

### Overview - Functions

```makefile
quickstart:
	make quickstart-setup
	make quickstart-tests

quickstart-setup:
	@echo "Quickstart Setup >> Started"
	make docker
	make validate
	make compile
	@echo "Quickstart Setup >> Finished"

quickstart-tests:
	make quickstart-setup
	make test-sanity
	make test-smoke
	make report
	make report-serve
```

### Overview - Commands

```makefile
docker:
	docker-compose up -d

validate:
	docker-compose exec -T dtective mvn -s /dtective/settings.xml validate

compile:
	docker-compose exec -T dtective mvn -s /dtective/settings.xml compile

test-smoke:
	docker-compose exec -T dtective mvn -s /dtective/settings.xml test "-Dcucumber.filter.tags=@Sanity"

test-sanity:
	docker-compose exec -T dtective mvn -s /dtective/settings.xml test "-Dcucumber.filter.tags=@Smoke"

report:
	docker-compose exec -T dtective mvn -s /dtective/settings.xml allure:report

report-serve:
	open allure serve ./target/site/allure-maven-plugin/****
```

## Testing Strategies

### Behavior Driven Testing

Please see https://en.wikipedia.org/wiki/Behavior-driven_development

### Data Driven Testing

Please see https://en.wikipedia.org/wiki/Data-driven_testing

### Performance Testing

Please see https://matyas-banszki-test-assignments.gitlab.io/project-columba-livia/testing/non-functional/

### Test Suites

#### Sanity Testing

Please see https://matyas-banszki-test-assignments.gitlab.io/project-columba-livia/testing/sanity/

#### Smoke Testing

Please see https://matyas-banszki-test-assignments.gitlab.io/project-columba-livia/testing/smoke/

#### Regression Testing

Please see https://matyas-banszki-test-assignments.gitlab.io/project-columba-livia/testing/regression/

## General Strategies

## Context-Driven Strategies

### Compliance Tests

Please see: https://matyas-banszki-test-assignments.gitlab.io/project-columba-livia/testing/compliance/

### Data Lifecycle and Data Driven Tests

Data driven testing is applied across the example!
For more, please see: https://matyas-banszki-test-assignments.gitlab.io/project-columba-livia/crud/

#### Mobile Platform Support

Please see: https://matyas-banszki-test-assignments.gitlab.io/project-columba-livia/testing/mobile/

## Test Automation

Cucumber - https://en.wikipedia.org/wiki/Cucumber_(software)

Please see : https://matyas-banszki-test-assignments.gitlab.io/project-columba-livia/testing/automation/

- This methodology describes all manual and automated tests.
- Test automation of these scripts is achieved through JAVA-MAVEN and https://cucumber.io/ and https://www.selenium.dev/

Example framework and documentation : http://dtective.io/

### Test Design and Development

- Suggested IDE: https://www.jetbrains.com/idea/
- Suggested Plugin: https://plugins.jetbrains.com/plugin/7212-cucumber-for-java

Please see: https://matyas-banszki-test-assignments.gitlab.io/project-columba-livia/tools/jetbrains/

# Test Files

## Features

| Category     | File                       | Intent                                |
| -----------  | -----------                | -----------                           |
| Example      | Example.feature            | Simple Behavior Driven Test           |
| Login        | Negative.feature           | Ensuring things can't happen          |
| Login        | Positive.feature           | Ensuring things can happen            |
| Registration | BasicCharacters.feature    | Quick most commonly used sanity test  |
| Registration | Special.feature            | UTF-8 coverage                        |
| Registration | DataValidation.feature     | Validation rules and error messages   |

## Page-Objects

| Category     | File                       | Intent                                |
| -----------  | -----------                | -----------                           |
| Custom       | Navigation.java            | Environment variable based target url |
| Login        | LoginSteps.java            | Bindings and Keyword Definitions      |
| Registration | RegistrationSteps.java     | Bindings and Keyword Definitions      |

# Tools and Technologies

## Docker Integration

Please see : https://matyas-banszki-test-assignments.gitlab.io/project-columba-livia/tools/docker

Setup instructions:

1. create .env from example or set up environment variables
    1. Set target URL for the tests
    2. Ensure **Gitlab Package Manager API Access**

Please use **Makefile** methods for easier access.

### Overview of Phases

#### Docker SETUP

```makefile
docker:
	docker-compose up -d
```

#### Selenium GRID setup

Please see https://aerokube.com/selenoid/latest/

Easy deploy:

- `docker pull selenoid/vnc:chrome_92.0`
- `docker pull selenoid/video-recorder:latest-release`
- `docker-compose -f selenoid.docker-compose.yml up -d --build`

The video recordings rely on the full path to the volume!

```yaml
environment:
  - OVERRIDE_VIDEO_OUTPUT_DIR=___PLEASE_CHANGE_ME___
```

In the `/config` folder, you will find an example `SeleniumCapabilities.yml.example.docker`
Please rename to `SeleniumCapabilities.yml` as this is linking the service to Selenoid

#### Docker VALIDATE

```makefile
validate:
	docker-compose exec -T dtective mvn -s /dtective/settings.xml validate
```

#### Docker COMPILE

```makefile
compile:
	docker-compose exec -T dtective mvn -s /dtective/settings.xml compile
```

#### Docker TEST

Feel free to change the @Sanity or @Smoke or combine them via https://cucumber.io/docs/cucumber/api/

```makefile
test-smoke:
	docker-compose exec -T dtective mvn -s /dtective/settings.xml test "-Dcucumber.filter.tags=@Sanity"

test-sanity:
	docker-compose exec -T dtective mvn -s /dtective/settings.xml test "-Dcucumber.filter.tags=@Smoke"

```

#### Report SERVE

## Selenium Integration

Please see: https://matyas-banszki-test-assignments.gitlab.io/project-columba-livia/tools/selenium/

## ELK Integration

Please see: https://matyas-banszki-test-assignments.gitlab.io/project-columba-livia/tools/elk/

## Gitlab Integration

Please see: https://matyas-banszki-test-assignments.gitlab.io/project-columba-livia/tools/gitlab/

## IntelliJ Integration

Please see: https://matyas-banszki-test-assignments.gitlab.io/project-columba-livia/tools/jetbrains/

## Google Lighthouse Integration

Please see: https://matyas-banszki-test-assignments.gitlab.io/project-columba-livia/tools/google/

## Maven Integration

K Please see: https://matyas-banszki-test-assignments.gitlab.io/project-columba-livia/tools/apache/

## Sitespeed Integration

Please see: https://matyas-banszki-test-assignments.gitlab.io/project-columba-livia/tools/sitespeed/

# Analytics

## Framework Analytics

Please see: https://matyas-banszki-test-assignments.gitlab.io/project-columba-livia/analytics/framework