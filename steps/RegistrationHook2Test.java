import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;


/**
 * Hook for JUnit.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"features/registration/DataDrivenTesting/validation"},
        glue = {"classpath:io.dtective", "navigation", "registration", "login"},
        plugin = {
                "io.qameta.allure.cucumber6jvm.AllureCucumber6Jvm",
                "json:target/cucumber-report/report.json",
                "io.dtective.webdriver.features.cucumber.ScenarioFinsihedListener"
        })
public class RegistrationHook2Test {
}
