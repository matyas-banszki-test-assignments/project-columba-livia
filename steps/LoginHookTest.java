import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;


/**
 * Hook for JUnit.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"features/login"},
        glue = {"classpath:io.dtective", "navigation", "registration", "login"},
        plugin = {
                "io.qameta.allure.cucumber6jvm.AllureCucumber6Jvm",
                "json:target/cucumber-report/report.json",
                "io.dtective.webdriver.features.cucumber.ScenarioFinsihedListener"
        })
public class LoginHookTest {
}
