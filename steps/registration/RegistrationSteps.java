package registration;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.dtective.webdriver.selenium.steps.asserts.WebdriverAssertSteps;
import io.dtective.webdriver.selenium.steps.clear.WebdriverClearSteps;
import io.dtective.webdriver.selenium.steps.click.WebdriverClickSteps;
import io.dtective.webdriver.selenium.steps.sendkeys.WebdriverSendKeysSteps;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class RegistrationSteps {

    /**
     * Helper Classes
     */
    public static final String pattern = "yyyy-MM-dd_HH-mm-ss";
    /**
     * Page Objects
     */
    public static final String SELECTOR_BTN_REGISTER = "//a[@href='/accounts/register/']";
    public static final String SELECTOR_BTN_SUBMIT = "//input[@type='submit']";
    public static final String SELECTOR_FIELD_USERNAME = "//input[@id='id_username']";
    public static final String SELECTOR_FIELD_PASSWORD = "//input[@id='id_password1']";
    public static final String SELECTOR_FIELD_PASSWORD2 = "//input[@id='id_password2']";

    /**
     * Static Data
     */
    public static final String URL_ROUTE_ACCOUNT_CREATED = "/feeds/";
    public static final String CONTENT_ACCOUNT_CREATED_TITLE = "//*[text()='Great success! Enjoy :)']";
    public static final String VALIDATION_REQUIRED_TEMPLATE = "//div[@id='div_id_%s']//*[@id='error_1_id_%s']";

    /**
     * Session Variables
     */
    private static String sessionUser;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
    /**
     * Test Framework Selenium Interface
     */
    private final WebdriverClickSteps clickSteps = new WebdriverClickSteps();
    private final WebdriverSendKeysSteps sendKeysSteps = new WebdriverSendKeysSteps();
    private final WebdriverAssertSteps assertSteps = new WebdriverAssertSteps();
    private final WebdriverClearSteps clearSteps = new WebdriverClearSteps();

    public static String getSessionUser() {
        return sessionUser;
    }

    /*
     * Page Functions
     */

    @When("I click on registration")
    public void clickRegister() {
        clickSteps.byXpath(SELECTOR_BTN_REGISTER);
    }

    @And("I type {string} into User Name")
    public void typeEmail(String email) {
        sendKeysSteps.byXpath(email, SELECTOR_FIELD_USERNAME);
    }

    @And("I clear User Name")
    public void clearEmail() {
        clickSteps.byXpath(SELECTOR_FIELD_USERNAME);
    }

    @And("I type randomized {string} into Email Address")
    public void typeRandomEmail(String email) {

        typeEmail(sessionUser);
    }


    @And("I type {string} into Password")
    public void typePassword(String pass) {
        sendKeysSteps.byXpath(pass, SELECTOR_FIELD_PASSWORD);
    }

    @And("I type {string} into Password Confirmation")
    public void typePasswordConfirmation(String pass) {
        sendKeysSteps.byXpath(pass, SELECTOR_FIELD_PASSWORD2);
    }

    @And("I click Create Account")
    public void clickCreateAccount() {
        clickSteps.byXpath(SELECTOR_BTN_SUBMIT);
    }

    // Page Function Aggregation

    @And("I register account with Email {string} Password {string} and Full Name {string} {string}")
    public void register(String email, String pass) {
        clickRegister();
        typeEmail(email);
        typePassword(pass);
        typePasswordConfirmation(pass);
        clickCreateAccount();

        assertAccountCreatedSuccess();
    }

    @And("I register account with Unique User {string} and Password {string}")
    public void registerUnique(String email, String pass) {
        generateSessionUser(email);
        register(sessionUser, pass);
    }

    @Then("I assert that the account registration is successful")
    public void assertAccountCreatedSuccess() {
        assertSteps.assertElementDisplaysByXpath(CONTENT_ACCOUNT_CREATED_TITLE);
        assertSteps.iAssertThatURLContains(URL_ROUTE_ACCOUNT_CREATED);
    }

    // Code Helpers
    private void generateSessionUser(String user) {
        String randomPrefix = dateFormat.format(Calendar.getInstance().getTime());
        sessionUser = randomPrefix + user;
    }

    @Then("I assert Required Field validation error for Username displays")
    public void iAssertRequiredFieldValidationErrorForUsernameDisplays() {
        assertSteps.assertElementDisplaysByXpath(String.format(VALIDATION_REQUIRED_TEMPLATE, "username", "username"));
    }

    @Then("I assert Required Field validation error for Password displays")
    public void iAssertRequiredFieldValidationErrorForPassowordDisplays() {
        assertSteps.assertElementDisplaysByXpath(String.format(VALIDATION_REQUIRED_TEMPLATE, "password1", "password1"));
    }

    @Then("I assert Required Field validation error for Password Confirmation displays")
    public void iAssertRequiredFieldValidationErrorForPassowordConfirmationDisplays() {
        assertSteps.assertElementDisplaysByXpath(String.format(VALIDATION_REQUIRED_TEMPLATE, "password2", "password2"));
    }
}
