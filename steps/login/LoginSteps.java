package login;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.dtective.webdriver.selenium.steps.asserts.WebdriverAssertSteps;
import io.dtective.webdriver.selenium.steps.click.WebdriverClickSteps;
import io.dtective.webdriver.selenium.steps.sendkeys.WebdriverSendKeysSteps;
import registration.RegistrationSteps;

public class LoginSteps {

    /**
     * Page Objects
     */
    public static final String SELECTOR_BTN_LOGIN = "//a[@href='/accounts/login/']";
    public static final String SELECTOR_BTN_LOGIN_SUBMIT = "//input[@type='submit']";
    public static final String SELECTOR_BTN_LOGOUT = "//a[@href='/accounts/logout/']";
    public static final String SELECTOR_FIELD_USER = "//input[@id='id_username']";
    public static final String SELECTOR_FIELD_PASSWORD = "//input[@id='id_password']";
    public static final String CONTENT_LOGIN_FAIL = "Please enter a correct username and password. Note that both fields may be case-sensitive";
    /**
     * Test Framework Selenium Interface
     */
    private final WebdriverClickSteps clickSteps = new WebdriverClickSteps();
    private final WebdriverSendKeysSteps sendKeysSteps = new WebdriverSendKeysSteps();
    private final WebdriverAssertSteps assertSteps = new WebdriverAssertSteps();

    /*
     * Page Functions
     */

    @And("I type {string} into Login User Name")
    public void typeUser(String user) {
        sendKeysSteps.byXpath(user, SELECTOR_FIELD_USER);
    }

    @And("I type unique name into Login User Name")
    public void typeSessionEmail() {
        sendKeysSteps.byXpath(RegistrationSteps.getSessionUser(), SELECTOR_FIELD_USER);
    }

    @And("I type {string} into Login Password")
    public void typePassword(String pass) {
        sendKeysSteps.byXpath(pass, SELECTOR_FIELD_PASSWORD);
    }

    @And("I click Login")
    public void clickLogin() {
        clickSteps.byXpath(SELECTOR_BTN_LOGIN);
    }

    @And("I click Execute Login")
    public void clickExecuteLogin() {
        clickSteps.byXpath(SELECTOR_BTN_LOGIN_SUBMIT);
    }

    // Page Function Aggregation

    @And("I login with User {string} Password {string}")
    public void login_aggregate(String user, String pass) {

        typeUser(user);
        typePassword(pass);
        clickExecuteLogin();
    }

    @And("I login with Unique User and Password {string}")
    public void login_aggregate_sessionUser(String pass) {
        login_aggregate(RegistrationSteps.getSessionUser(), pass);
    }

    @Then("I assert that wrong email or password message displays")
    public void iAssertThatWrongUserOrPasswordMessageDisplays() {
        assertSteps.assertTextDisplaysContaining(CONTENT_LOGIN_FAIL);
    }

    @Then("I click Logout")
    public void iClickLogout() {
        clickSteps.byXpath(SELECTOR_BTN_LOGOUT);
    }
}
