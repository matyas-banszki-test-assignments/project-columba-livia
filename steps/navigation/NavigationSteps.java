package navigation;

import io.cucumber.java.en.Given;
import io.dtective.webdriver.selenium.steps.navigation.WebdriverNavigateSteps;

public class NavigationSteps {

    private static final String ENV_TEST_TARGET = "TEST_TARGET";
    private static String URL = "http://49.12.204.185:8000";

    static {
        String envVar = System.getenv(ENV_TEST_TARGET);
        URL = envVar == null || envVar.isBlank() ? URL : envVar;
    }

    private final WebdriverNavigateSteps navigationSteps = new WebdriverNavigateSteps();

    @Given("I navigate to test application")
    public void iNavigateToTestApplication() {
        navigationSteps.to(URL);
    }

}
