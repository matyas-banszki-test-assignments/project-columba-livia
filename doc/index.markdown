---
layout: default
title: Home
nav_order: 1
description: "Just the Docs is a responsive Jekyll theme with built-in search that is easily customizable and hosted on GitHub Pages."
permalink: /
---

# Welcome

---
“It is not the critic who counts; not the man who points out how the strong man stumbles, or where the doer of deeds
could have done them better. The credit belongs to the man who is actually in the arena, whose face is marred by dust
and sweat and blood; who strives valiantly; who errs, who comes short again and again, because there is no effort
without error and shortcoming; but who does actually strive to do the deeds; who knows great enthusiasms, the great
devotions; who spends himself in a worthy cause; who at the best knows in the end the triumph of high achievement, and
who at the worst, if he fails, at least fails while daring greatly, so that his place shall never be with those cold and
timid souls who neither know victory nor defeat.” 
{: style=" font-size: 80%; text-align: center;"}

[Theodore Roosevelt](https://en.wikipedia.org/wiki/Theodore_Roosevelt)
{: style=" font-size: 80%; text-align: center;"}
---

### Mission

---
Quality assurance through definition and continuous assessment, improvement of communication, implementation and
operation through high standards and principled engineering!
{: style=" font-size: 110%; text-align: center;"}
---

### But Why?

---
Any bug, flaw, error or negative user/operational/development experience produced by lack of or incorrect, insufficient
use of these impact the very first step of development, **conception**. A reminder I use myself to maintain balance:

- `To do the right things`
- `To do the things right`

The first one focuses on selecting and always making progress towards a worthwhile cause, the latter one emphasizes that
the journey to an objective is an essential component of it.

---

![avatar-matyas.png](/project-columba-livia/assets/images/avatar-matyas.png)
{: style=" font-size: 80%; text-align: center;"}

### Important Notice

The build process relies access to an internal package registry. Keys are provided on request. Currently, without this
the project cannot be built, automation cannot be executed.

### References

- [Gitlab HOME](https://gitlab.com/matyas-banszki-test-assignments/project-columba-livia)
- [Gitlab README.md](https://gitlab.com/matyas-banszki-test-assignments/project-columba-livia/-/blob/master/README.md)
- [https://cucumber.io/](https://cucumber.io/)
- [https://en.wikipedia.org/wiki/Behavior-driven_development](https://en.wikipedia.org/wiki/Behavior-driven_development)
- [http://dtective.io/](http://dtective.io/)
