---
layout: default
title: Sitespeed.io
nav_order: 6
parent: Tooling Home
permalink: /tools/sitespeed/
description: "Sitespeed.io Grafana Testing!"

---

## Sitespeed.io Performance Testing

[https://www.sitespeed.io/documentation/sitespeed.io/performance-dashboard/](https://www.sitespeed.io/documentation/sitespeed.io/performance-dashboard/)

```shell
docker run --shm-size 2g --rm -v "$(pwd):/sitespeed.io" sitespeedio/sitespeed.io:19.2.0 --graphite.host=host.docker.internal -b chrome https://MASKED_URL --slug project-nubes-aedificationem-login-2 --graphite.addSlugToKey true
```

![image tooltip here](/project-columba-livia/assets/images/sitespeed_grafana1.png)

![image tooltip here](/project-columba-livia/assets/images/sitespeed_grafana2.png)

![image tooltip here](/project-columba-livia/assets/images/sitespeed_grafana3.png)