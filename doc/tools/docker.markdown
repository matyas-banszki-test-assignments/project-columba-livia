---
layout: page
title: Docker
permalink: /tools/docker/
nav_order: 6
parent: Tooling Home
---


## Pre-Requisites

The build process relies access to an internal package registry. Keys are provided on request. **Cannot be built without
access.**

## HELP

The container is designed to run as a service inside docker awaiting your commands.

## Cheat Sheet

- Startup - `docker-compose up -d`
- Verify - `docker-compose exec -T dtective mvn -q -s /dtective/settings.xml validate`
- Compile - `docker-compose exec -T dtective mvn -q -s /dtective/settings.xml compile`
- Test - `docker-compose exec -T dtective mvn -q -s /dtective/settings.xml test -Dcucumber.filter.tags=@Smoke`
- Report - `docker-compose exec -T dtective mvn -q -s /dtective/settings.xml allure:report`

The HTML test report requires a web-server. Suggestions:

- Install ALLURE command line tool https://docs.qameta.io/allure/
- Use IntelliJ IDEA and follow the instructions below:

![image tooltip here](/project-columba-livia/assets/images/docker-report-help.png)
