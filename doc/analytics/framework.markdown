---
layout: page title: Framework Analytics permalink: /analytics/framework parent: Analytics Home nav_order: 7
---

## Data Driven Decisions!

The framework is designed to outsource the handled data and events to a processing platform like
ELK. [https://www.elastic.co/what-is/elk-stack](https://www.elastic.co/what-is/elk-stack)
