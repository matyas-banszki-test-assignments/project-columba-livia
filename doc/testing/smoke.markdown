---
layout: default
title: Smoke
nav_order: 5
parent: Testing Home
description: "Smoke Testing!"

---
## Smoke Testing

---

"Finding the easiest, cheapest way to access and make use of most of the system or its parts prone to error and listen
for explosions, watch out for smoke!"
{: style=" font-size: 80%; text-align: center;"}

---

Guidelines:

- Amount - `some`
- Complexity - `Low`
- Execution Cost / Estimate - `Low`

1. Limited Product, Content and User Experience Tests
2. Business value based requirement coverage via mostly automated, some manual testing
3. Exploratory tests
4. Limited test automation and alerting systems reliability checks
5. Limited Functional and Non-Functional testing
