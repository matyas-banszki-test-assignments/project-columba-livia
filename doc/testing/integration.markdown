---
layout: default
title: Integration
nav_order: 6
parent: Testing Home
description: "Integration Testing!"

---

## Component and Integration Testing

---

#### OPS Component

- Website and Service availability, observability, monitoring, alerting and health-checks
-

---

#### Web Component

"Industry standards and checking for potential areas of improvement!"
{: style=" font-size: 80%; text-align: center;"}

TBD

---

#### Web Integration

"How we send, receive and transfer messages through the cloud and has some rules and best practices, you know!"
{: style=" font-size: 80%; text-align: center;"}

TBD

---

#### Service Integration

"With the relationship of services and websites, a lot can go wrong including them not talking at all to each other!"
{: style=" font-size: 80%; text-align: center;"}

N/A

---

#### Service Testing

"Users don't even know what they are, let's trust and assume in our context that these work every time, all the time ;)"
{: style=" font-size: 80%; text-align: center;"}

N/A

---
