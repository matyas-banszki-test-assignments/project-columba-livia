---
layout: default
title: Testing Home
nav_order: 4
description: "Test Management Home"
has_children: true
has_toc: true
---

# Testing Strategies, Suites, Categories

---

_"there are more than 150 types of testing types and still adding. Also, note that not all testing types are applicable
to all projects but depend on the nature & scope of the project."_
{: style=" font-size: 80%; text-align: center;"}

[Software Testing - Guru99](https://www.guru99.com/software-testing-introduction-importance.html)
{: style=" font-size: 80%; text-align: center;"}

## Test Reports
 
- [Test Report Demo - Example.feature](/project-columba-livia/assets/reports/example/allure/)
- [Test Report 1 - Registration and Login](/project-columba-livia/assets/reports/1/allure/)