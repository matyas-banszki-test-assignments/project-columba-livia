---
layout: default
title: Non-Functional
nav_order: 6
parent: Testing Home
description: "Non-Functional Testing!"

---

## Non-Functional Testing


---

#### Security

"Someone mentioned passwords? I am collecting them for my university research study, may I have yours?"
{: style=" font-size: 80%; text-align: center;"}

Please see page `Zap Report` page

#### Performance

"Between searching and getting the results, I feel like I can watch the movie Titanic"
{: style=" font-size: 80%; text-align: center;"}

Lighthouse + ELK + GTmetrix + WebPageTest