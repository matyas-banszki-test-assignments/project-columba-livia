---
layout: default
title: Test Automation
nav_order: 6
parent: Testing Home
description: "Test Automation!"

---

## Test Automation with Cucumber

### Pre-built Script library

To get started, methods of using a browser are already implemented for you
using [https://www.selenium.dev/](https://www.selenium.dev/)
that automates testing in virtual browsers is decoupled and comes with a layer requiring `No Code or Coding Knowledge`.

_"I cannot code, how do I automate?"_
{: style=" font-size: 100%; text-align: center;"}

It takes, three major components but everyone has them!

1. Ability to communicate intent using a language! **Check**.
2. Ability to use a Web Browser to do things e.g.: Chrome. **Check**.
3. Access to [IntelliJ IDEA](https://www.jetbrains.com/idea/download) **Check**.

### Flexibility by design

The combination of [Abstraction](https://en.wikipedia.org/wiki/Abstraction_principle_(computer_programming))
, [Inversion of Control(IoC)](https://en.wikipedia.org/wiki/Inversion_of_control)
and [Separation of Concern(SoC)](https://en.wikipedia.org/wiki/Separation_of_concerns)
allows the software to dynamically adjust the classes determining execution code at runtime to
achieve [Strategy Pattern](https://en.wikipedia.org/wiki/Strategy_pattern)

### Code-Less Test Automation

Executable example : `/features/demo/example.feature`
The currently used implementation of the code is not an extension of, has no relation to http://dtective.io/ other than
technological stack and motivation to establish tool-aided communication. Methods below may not be accessible for
automation demonstration, they are here to demonstrate what can, should be tested

The following script together with the packaged or dependency based framework can be executed.

To get started, put this into a `*.feature` file and add the `pom.xml` and load the project with `IntelliJ IDEA`

- `*.feature` represent test files
- `pom.xml` is the project object model and it includes settings, dependencies and the build process.

The integration and usage of the included test framework will be updated to use a compiled JAR version and a custom
entrypoint as it becomes available in its next release.

```gherkin
Feature: User Registration

  Scenario: Basic Registration
    Given I navigate to "http://URL"
    When I click by text "Register"
    And I type "my-test" into field by xpath "//input[@name='first_name']"
    And I type "account" into field by xpath "//input[@name='last_name']"
    And I type "example@dtective.club" into field by xpath "//input[@name='email_address']"
    And I type "Pass1wd!" into field by xpath "//input[@name='password']"
    And I click by text "Create Account"
    And I assert that text displays containing "Success"
```

## Automation DSL Library

#### Web Data Management

```gherkin
  # --------------------------------------------------------------------------------------------------------------------
  # Code-Less Selenium Interface Automation DSL
  # --------------------------------------------------------------------------------------------------------------------
Feature: Web Data Management
  
  # https://en.wikipedia.org/wiki/HTTP_cookie
  Scenario: Site Cookies
    Given I delete all cookies
    When I add cookie "test" with value "1"
    Then I assert that cookie with name "test" exists
    And I assert that cookie with name "test" has value "1"
    And I delete cookie "test"
    Then I assert that cookie key with name "test" does not exist
    
  # https://en.wikipedia.org/wiki/Web_storage
  Scenario: Local Storage
    Given I delete all local storage
    When I add to local storage "test" with value "1"
    Then I assert that local storage key with name "test" exists
    And I assert that local storage key with name "test" has value "1"
    And I delete local storage key "1"
    Then I assert that local storage key with name "test" does not exist
    
  # https://en.wikipedia.org/wiki/Web_storage
  Scenario: Session Storage
    When I add to session storage "test" with value "1"
    Then I assert that session storage key with name "test" exists
    And I assert that session storage key with name "test" has value "1"
    And I delete session storage key "test"
    Then I assert that session storage key with name "test" does not exist
```

#### Web CSS Style Test Automation

```gherkin
  Feature: CSS Testing

  Scenario Outline: CSS Testing
    Then I assert that the element with attribute "name" Value "Login-Button" has CSS attribute "<CSS_KEY>" with CSS value "<CSS_VALUE>"
    Examples:
      | CSS_KEY          | CSS_VALUE              |
      | color            | #fff                   |
      | font-size        | 16px                   |
      | background-color | #acb3bf                |
      | color            | rgb(0, 0, 0)           |
      | background-color | rgb(172, 179, 191)     |
      | background-color | rgba(172, 179, 191, 1) |
```

#### Advanced Mouse Control - Hover

```gherkin
Feature: Advanced Mouse Control - Hover

  Scenario : Color Change event via Mouse Hover

    Then I assert that the element with attribute "type" Value "submit" has background colour #"26a69a"
    When I hover over element with Property "type" and Value "submit"
    And I wait "500" ms
    Then I assert that the element with attribute "type" Value "submit" has background colour #"2bbbad"
```

#### Advanced Mouse Control - Scroll to Element

```gherkin
Feature: Advanced Mouse Control - Scroll to Element

  Scenario: Element is not on screen, not interactable
    When I scroll to element by "type" value "submit"
    Then I click element by "type" value "submit"
```

#### In-Memory Clipboard Storage

```gherkin
Feature: In-Memory Data Storage - Clipboard

  Scenario: Copy & Paste
    Given I copy "Welcome" to the clipboard
    Given I open website "http://dtective.club"
    When I paste the value from clipboard to element by Attribute "id" Value "email"
```

#### Managing Web-Browser Tabs

```gherkin
Feature: Web-Browser Tab Control

  Scenario: Opening a new tab
    When I open link by xpath "//a[@href='/about']" in a new Tab
    And I switch to the newly opened tab
    And I close the current Tab

  Scenario: Opening links that create new tabs
    When I click element by "id" value "about"
    And I switch to the newly opened tab
    And I close the current Tab
```