---
layout: default
title: Regression
nav_order: 6
parent: Testing Home
description: "Regression Testing!"

---

## Regression Testing


---

"Every new feature or change shall be added on top of existing ones which we already have customers relying on. We will
protect these going forward"
{: style=" font-size: 80%; text-align: center;"}
---
