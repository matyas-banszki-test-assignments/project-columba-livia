---
layout: default
title: Sanity
nav_order: 4
parent: Testing Home
description: "Sanity Testing!"

---

## Sanity Testing

---
"If we deploy late, the product owner won't sleep well until he checked these!"
{: style=" font-size: 80%; text-align: center;"}

---

Guidelines:

- Amount - `Few`
- Complexity - `Very Low`
- Execution Cost / Estimate - `Negligible`

> Assuming monitoring and alerting is applied

**Basic Accessibility Tests**

- Web Server Responsiveness Tests (included in alerting!)
- Web Response Content tests ("_HTTP-200 and Cloudflare-Home-Page is not great user experience_")
- Web Page Content and Dependency tests ("_CDN, external sources for scripts or even internally hosted package managers
  can fail and leave half the website non-functional_")

**Basic Product, Content and User Experience Tests**

- Risk based requirement coverage via automated testing
- Automated testing, alerting on SEO, WEB standards checks https://developers.google.com/web/tools/lighthouse
- Automated testing, alerting on SEO, WEB content, analytics https://www.screamingfrog.co.uk/seo-spider/
- Automated testing, alerting on Security and WEB standards https://owasp.org/www-project-zap/
- Automated visual testing, alerting https://applitools.com/visual-testing/

**Time-boxed manual exploratory testing**

https://www.getxray.app/exploratory-testing
