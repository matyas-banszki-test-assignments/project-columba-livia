---
layout: default
title: Feeds
nav_order: 2
parent: Features Home
description: "Feeds"

---


## User Registration

- [Wikipedia - Functional Requirements](https://en.wikipedia.org/wiki/Functional_requirement)
- [Wikipedia - Non-Functional Requirements](https://en.wikipedia.org/wiki/Non-functional_requirement)

```text
    .
    ├── PRODUCT
    │   ├── Functional
    │   │    ├── Business Compliance
    │   │    └── Business Value
    │   └── Non-Functional
    │        ├── User, Developer, Operational Experience
    │        └── TBD     
    └── TECHNOLOGY
        ├── Functional
        │    ├── Technology Standards
        │    └── TBD 
        └── Non-Functional
             ├── Security
             ├── Performance
             └── TBD  
```

### Table of Content

* [Product Requirements](#product-requirements)
  * [Functional Requirements](#functional-business-requirements)
  * [Non-Functional Requirements](#non-functional-business-requirements)
* [Technical Requirements](#technical-requirements)
  * [Functional Requirements](#functional-technical-requirements)
  * [Non-Functional Requirements](#non-functional-technical-requirements)

## Product Requirements

### Functional Business Requirements

### Non-Functional Business Requirements

## Technical Requirements

### Functional Technical Requirements

### Generic Entity CRUD Technical Guideline

Please see PDF - [Extended CRUD Automation](/project-columba-livia/crud/)

### Non-Functional Technical Requirements


## Test Automation and Execution
