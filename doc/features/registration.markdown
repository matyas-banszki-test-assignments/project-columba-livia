---
layout: default
title: Registration
nav_order: 2
parent: Features Home
description: "Quality Assurance in Account Registration"

---

## User Registration

- [Wikipedia - Functional Requirements](https://en.wikipedia.org/wiki/Functional_requirement)
- [Wikipedia - Non-Functional Requirements](https://en.wikipedia.org/wiki/Non-functional_requirement)

```text
    .
    ├── PRODUCT
    │   ├── Functional
    │   │    ├── Business Compliance
    │   │    └── Business Value
    │   └── Non-Functional
    │        ├── User, Developer, Operational Experience
    │        └── TBD     
    └── TECHNOLOGY
        ├── Functional
        │    ├── Technology Standards
        │    └── TBD 
        └── Non-Functional
             ├── Security
             ├── Performance
             └── TBD  
```

### Table of Content

* [Product Requirements](#product-requirements)
  * [Functional Requirements](#functional-business-requirements)
  * [Non-Functional Requirements](#non-functional-business-requirements)
* [Technical Requirements](#technical-requirements)
  * [Functional Requirements](#functional-technical-requirements)
  * [Non-Functional Requirements](#non-functional-technical-requirements)

## Product Requirements

### Functional Business Requirements

```gherkin
Feature: User Access Control - Registration

  Scenario: Ability to access a webpage to register

  Scenario: Ability to proceed to the login page after a successful registration
```

### Non-Functional Business Requirements

```gherkin
#https://gdpr.eu/eu-gdpr-personal-data/
Feature: Business Compliance - GDPR

  Scenario: Ability to perform a GDPR assessment
  Scenario: Ability to review and keep track of GDPR assessments
  Scenario: Ability to engineer privacy by design
  Scenario: Ability to inform the user through a Privacy Policy
  Scenario: Ability to request informed consent from users prior to completion of registration or access of services
  Scenario: Ability to provide a users complete data portfolio on request
  Scenario: Ability to remove all data related to a user on request.
```

## Technical Requirements

### Functional Technical Requirements

```gherkin
Feature: Data Abstraction

  # https://en.wikipedia.org/wiki/Password
```

### Generic Entity CRUD Technical Guideline

Please see PDF - [Extended CRUD Automation](/project-columba-livia/crud/)

### Character Encoding

1. Data is TEXT
2. TEXT is defined as a sequence of characters
3. characters are subjected to
   encoding [https://en.wikipedia.org/wiki/Character_encoding](https://en.wikipedia.org/wiki/Character_encoding)

--- 

1. Individuals _(like me)_ may have special characters within their names.
2. Unless restricted, advised against, their entry is by default allowed
3. Even if they are business-wise determined as "Not Supported", it is a topic for consideration about user experience
4. When supported, data driven testing can ensure technical and contextual, edge cases

Please
see [SpecialCharacters.feature](https://gitlab.com/matyas-banszki-test-assignments/project-columba-livia/-/blob/master/features/registration/DataDrivenTesting/input/SpecialCharacters.feature)

```gherkin
  Scenario Outline: Registration - Special Characters - UTF-8
When I register account with Unique User "<FIRST_NAME>+<LAST_NAME>" and Password "Pass1wd!"
Then I the account registration is successful

@UTF8
Examples:
| NATION | FIRST_NAME | LAST_NAME |
| A      | ÀÁÂÃÄÅÆ    | ÀÁÂÃÄÅÆ   |
| EI     | ÇÈÉÊËÌÍÎÏ  | ÇÈÉÊËÌÍÎÏ |

@ExampleNames
Examples:
| NATION     | FIRST_NAME | LAST_NAME  |
| Africa     | Sethunya   | Temitope   |
| Chinese    | 孫雅         | 恩        |
| Hungarian  | Bala       | Enikõ      |
| Japanese   | 餅田         | 美優       |
| Russian    | Белла      | Агафо́нова  |
| Vietnamese | Chu_Thị    | Mộng_Hà    |
```

### Non-Functional Technical Requirements

```gherkin
Feature: Accessibility

  Scenario: Ability to access the Register page using its full url
  Scenario: Ability to access the Register page from the Login page
  Scenario: Ability to redirect HTTP request to HTTPS
```

```gherkin
Feature: Security

  Scenario: Ability to hide or mask on-screen passwords by default
  Scenario: Ability to show on-screen passwords on request
  Scenario: Ability to restrict password syntax and inform the user of these rules
```

---

## Test Automation and Execution
