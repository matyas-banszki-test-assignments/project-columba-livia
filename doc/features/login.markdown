---
layout: default
title: Login
nav_order: 2
parent: Features Home
description: "User Authentication - Login"

---

## User Login

- [Wikipedia - Functional Requirements](https://en.wikipedia.org/wiki/Functional_requirement)
- [Wikipedia - Non-Functional Requirements](https://en.wikipedia.org/wiki/Non-functional_requirement)

```text
    .
    ├── PRODUCT
    │   ├── Functional
    │   │    ├── Business Compliance
    │   │    └── Business Value
    │   └── Non-Functional
    │        ├── User, Developer, Operational Experience
    │        └── TBD     
    └── TECHNOLOGY
        ├── Functional
        │    ├── Technology Standards
        │    └── TBD 
        └── Non-Functional
             ├── Security
             ├── Performance
             └── TBD  
```

### Table of Content

* [Product Requirements](#product-requirements)
    * [Functional Requirements](#functional-business-requirements)
    * [Non-Functional Requirements](#non-functional-business-requirements)
* [Technical Requirements](#technical-requirements)
    * [Functional Requirements](#functional-technical-requirements)
    * [Non-Functional Requirements](#non-functional-technical-requirements)

## Product Requirements

### Functional Business Requirements

```gherkin
Feature: User Access Control - Login

  Scenario: Ability to access a webpage to login
  Scenario: Ability to perform authentication using credentials
  Scenario: Ability to proceed to the application after a successful login
```

### Non-Functional Business Requirements

## Technical Requirements

### Functional Technical Requirements

```gherkin
Feature: Data Abstraction

  Scenario: Ability to validate email syntax and prevent invalid authentication requests

  Scenario: Ability to not validate the password length or complexity on login to hide this information
```

```gherkin
Feature: Process Abstraction

  Scenario: Ability to collect EMAIL and PASSWORD from a user
  Scenario: Ability to send EMAIL and PASSOWRD to indentity manager service for authentication
  Scenario: Ability to process the response from an identity manager service after authentication attempt
  Scenario: Ability to respond to an authentication request from a user 
```

### Non-Functional Technical Requirements

```gherkin
Feature: Accessibility

  Scenario: Ability to access the Login page using its full url
  Scenario: Ability to be redirected to the Login page upon accessing restricted pages without authorization
  Scenario: Ability to be redirected into the application landing page upon accessing the Login page with authorization
  Scenario: Ability to redirect HTTP request to HTTPS
```

```gherkin
Feature: Security

  Scenario: Ability to hide or mask on-screen passwords by default
  Scenario: Ability to show on-screen passwords on request
  Scenario: Ability to request a forgotten password
  Scenario: Ability to restrict password syntax
  Scenario: Ability to restrict reuse of previously used passwords
```

