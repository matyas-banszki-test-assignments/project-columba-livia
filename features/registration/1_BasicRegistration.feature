Feature: User Registration

  Scenario: Create the first user!
    Given I navigate to test application
    When I click by xpath "//a[@href='/accounts/register/']"
    When I type "dtective-38445425.club" into field by xpath "//*[@id='id_username']"
    When I type "pass1wd!" into field by xpath "//*[@id='id_password1']"
    When I type "pass1wd!" into field by xpath "//*[@id='id_password2']"
    When I click by xpath "//*[@id='submit-id-submit']"
    Then I assert that text displays containing "Great success! Enjoy :)"
    Then I assert that URL contains "/feeds"
    Then I assert that element displays by xpath "//*[@class='alert alert-success']"