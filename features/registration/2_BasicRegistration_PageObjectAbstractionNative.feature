Feature: User Registration 2

  Scenario Outline: Standardize your script bindinds
    Given I navigate to test application
    When I click by xpath "<XP_BTN><XP_BTN_REG>"
    When I type "dtective-3663453.club" into field by xpath "<XP_FIELD><XP_FIELD_USER>"
    When I type "pass1wd!" into field by xpath "<XP_FIELD><XP_FIELD_PASS>"
    When I type "pass1wd!" into field by xpath "<XP_FIELD><XP_FIELD_PASS2>"
    When I click by xpath "<XP_FIELD><XP-BTN-SUBMIT>"
    Then I assert that text displays containing "Great success! Enjoy :)"
    Then I assert that URL contains "/feeds"
    Then I assert that element displays by xpath "<XP_LBL_SUCCESS>"

    @Elements
    Examples:
      | XP_BTN | XP_FIELD | XP_FIELD_USER       | XP_FIELD_PASS        | XP_FIELD_PASS2       | XP-BTN-SUBMIT    | XP_BTN_REG                    | XP_LBL_SUCCESS                    |
      | //a    | //input  | [@id='id_username'] | [@id='id_password1'] | [@id='id_password2'] | [@type='submit'] | [@href='/accounts/register/'] | //*[@class='alert alert-success'] |
