Feature: User Registration 2

  Background: Test Preparation
    Given I navigate to test application

  @CI
  Scenario: Basic Registration - Page Object CODE Abstraction
    When I click on registration
    And I type "My-Test-User-873" into User Name
    And I type "Pass1wd!" into Password
    And I click Create Account
    And I wait 3000 ms
    And I type "My-Test-User-873" into Login User Name
    And I type "Pass1wd!" into Password
    And I click Login
    Then I assert that text displays containing "Nothing to see here. Move on!"

