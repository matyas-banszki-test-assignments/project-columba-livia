@Smoke
Feature: User Registration  - Data Driven Testing

  Background: Test Preparation
    Given I navigate to test application

  @SpecialCharacters
  Scenario Outline: Registration - Special Characters - UTF-8
    When I register account with Unique User "<FIRST_NAME>+<LAST_NAME>" and Password "Pass1wd!"
    Then I click Logout

    @UTF8
    Examples:
      | FIRST_NAME | LAST_NAME |
      | ÀÁÂÃÄÅÆ    | ÀÁÂÃÄÅÆ   |
      | ÇÈÉÊËÌÍÎÏ  | ÇÈÉÊËÌÍÎÏ |
    # To be continued...

    @ExampleNames
    Examples:
      | NATION     | FIRST_NAME | LAST_NAME  |
      | Africa     | Sethunya   | Temitope   |
      | Chinese    | 孫雅         | 恩          |
      | Hungarian  | Bala       | Enikõ      |
      | Japanese   | 餅田         | 美優         |
      | Russian    | Белла      | Агафо́нова |
      | Vietnamese | Chu_Thị    | Mộng_Hà    |
    #To be automated using an online name registry, generator
