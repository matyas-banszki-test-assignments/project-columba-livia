@Smoke
Feature: User Registration  - Data Driven Testing

  Background: Test Preparation
    Given I navigate to test application

  Scenario Outline: Basic Registration - Name Tests
    When I register account with Unique User "<FIRST_NAME>_<LAST_NAME>" and Password "Pass1wd!"
    Then I click Logout
    Examples:
      | FIRST_NAME      | LAST_NAME   |
      | John            | Doe         |
      | Jane            | Doe         |
      | qwertyuiopasdfg | hjklzxcvbnm |
      | QWERTYUIOPASDFG | HJKLZXCVBNM |
