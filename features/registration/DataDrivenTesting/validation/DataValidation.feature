@Smoke
Feature: User Registration

  Background: Test Preparation
    Given I navigate to test application
    And I click on registration

  Scenario: Data Validation - Required Fields
    When I click Create Account
    Then I assert Required Field validation error for Username displays
    And I assert Required Field validation error for Password displays
    And I assert Required Field validation error for Password Confirmation displays

  Scenario Outline: Data Validation - User Name Format
    Given I clear User Name
    When I type "<USER>" into User Name
    And I click Create Account
    Then I assert Required Field validation error for Username displays
    Examples:
      | USER         |
      | @.*^com      |
      | A@//M<A      |
      | te~!#st.com@ |
      |              |
