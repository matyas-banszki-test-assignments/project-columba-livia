Feature: User Registration 2

  Background: Test Preparation
    Given I navigate to test application

  Scenario Outline: Basic Registration - Data Abstraction
    When I click on registration
    And I type "<NAME>" into User Name
    And I type "<PASS>" into Password
    And I type "<PASS>" into Password Confirmation
    And I click Create Account
    Then I assert that text displays containing "Great success! Enjoy :)"
    And I click Logout

    Examples:
      | NAME    | PASS     |
      | testx11 | Pass1wd! |
      | testx12 | Pass1wd! |
      | testx13 | Pass1wd! |
      | testx14 | Pass1wd! |
      | testx15 | Pass1wd! |
      | testx16 | Pass1wd! |
      | testx17 | Pass1wd! |

