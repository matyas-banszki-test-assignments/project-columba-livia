@Smoke
Feature: User Login - Data Driven Testing

  Background: Test Preparation
    Given I navigate to test application

  @Negative
  Scenario Outline: Negative - Not existing accounts
    Given I click Login
    When I type "<USER>" into Login User Name
    And I type "<PASS>" into Login Password
    And I click Execute Login
    Then I assert that wrong email or password message displays
    And I assert that URL contains "/login"
    Examples:
      | USER                      | PASS     |
      | 2022_testing_i-dont-exist | Pass1wd! |

  # Run only once or when necessary
  @Setup
  Scenario Outline: Negative - SETUP - <SCENARIO>
    Given I register account with Unique User "<USER>" and Password "<PASS>"
    Then I click Logout
    Examples:
      | SCENARIO                        | USER                           | PASS     |
      | Existing Account Negative Tests | login-exists-test-1@online.com | Pass1wd! |

  @Negative
  Scenario Outline: Negative - Existing Accounts Invalid Password
    Given I click Login
    When I type unique name into Login User Name
    And I type "<PASS>" into Login Password
    And I click Execute Login
    Then I assert that wrong email or password message displays
    And I assert that URL contains "/login"
    Examples:
      | PASS     |
      | test1234 |
      | -------- |
      | PASS1WD! |
      | Pass1wd  |