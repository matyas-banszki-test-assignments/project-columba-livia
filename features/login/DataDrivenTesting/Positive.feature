@Smoke
Feature: User Login - Data Driven Testing

  Background: Test Preparation
    Given I navigate to test application

  Scenario Outline: Data Driven Tests - "<DESC>"
    When I register account with Unique User "<USER>" and Password "Pass1wd!"
    Then I click Logout
    And I click Login
    Then I login with Unique User and Password "Pass1wd!"
    And I click Logout
    Examples:
      | DESC            | USER                                                |
      | Basic email     | test1324                                            |
      | Basic email     | automation                                          |
      | Basic Long User | test1324test1324test1324test1324test1324test1324    |
      | Basic Long User | test1324@test12345678901234567890123456789012345678 |