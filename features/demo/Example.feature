Feature: Dtective Code-Less Scripting Example

  # XPATH / CSS Selectors
  #    These are bindings, a contract between the test and the target page, product that enables automated user behavior replication.

  # NOTE
  # The following methods are for demonstration of tool capability. They can be changed, overridden and expanded on in any library that is included.
  # The code example of how to achieve such is already demonstrated in "Data Driven Testing" of features in this repository
  # For further examples, see https://matyas-banszki-test-assignments.gitlab.io/project-columba-livia/ or http://dtective.io/

  # --------------------------------------------------------------------------------------------------------------------
  # QUICKSTART AUTOMATION DSL
  # --------------------------------------------------------------------------------------------------------------------

  @Sanity
  Scenario: Create the first user!
    Given I navigate to test application
    When I click by xpath "//a[@href='/accounts/register/']"
    When I type "dtective-36863425.club" into field by xpath "//*[@id='id_username']"
    When I type "pass1wd!" into field by xpath "//*[@id='id_password1']"
    When I type "pass1wd!" into field by xpath "//*[@id='id_password2']"
    When I click by xpath "//*[@id='submit-id-submit']"
    Then I assert that text displays containing "Great success! Enjoy :)"
    Then I assert that URL contains "/feeds"
    Then I assert that element displays by xpath "//*[@class='alert alert-success']"


