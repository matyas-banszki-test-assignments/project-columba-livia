HELL=/bin/bash   # This is the standard compliant method

default: help

help:
	@echo "Please visit >> https://matyas-banszki-test-assignments.gitlab.io/project-columba-livia/"

quickstart:
	make quickstart-setup
	make quickstart-tests

quickstart-setup:
	@echo "Quickstart Setup >> Started"
	make docker
	make validate
	make compile
	@echo "Quickstart Setup >> Finished"

quickstart-tests:
	make quickstart-setup
	make test-sanity
	make test-smoke
	make report
	make report-serve

docker:
	docker-compose up -d

validate:
	docker-compose exec -T dtective mvn -U -q -s /dtective/settings.xml validate

compile:
	docker-compose exec -T dtective mvn -q -s /dtective/settings.xml compile

test-sanity:
	docker-compose exec -T dtective mvn -q -s /dtective/settings.xml test "-Dcucumber.filter.tags=@Sanity"

test-smoke:
	docker-compose exec -T dtective mvn -q -s /dtective/settings.xml test "-Dcucumber.filter.tags=@Smoke"

report:
	docker-compose exec -T dtective mvn -q -s /dtective/settings.xml allure:report

report-serve:
	@echo "Allure needs to be installed"
	#open allure serve ./target/site/allure-maven-plugin/